# NScript
基于 .Net 框架的用来替代 vbs 的 Windows 脚本工具集;

# 脚本宿主和相关文件

* wnscript.exe - .Net Windows 脚本宿主;
* cnscript.exe - .Net Console 脚本宿主;
* checkpath.exe - 命令行工具,检查命令的文件路径;

# 脚本扩展名

* *.vbx - Visual Basic .Net 脚本文件;
* *.csx - C# .Net 脚本文件;

# 安装 NScript

* 安装需求: [.Net Framework 4.0](https://www.microsoft.com/zh-CN/download/details.aspx?id=17718)
* [下载安装文件;](https://gitee.com/milaoshu1020/NScript/releases)
* 双击 NScript_XXX_Setup.exe 进行安装;

# 编辑脚本文件

* 你可以使用记事本或者其他文本编辑器编辑脚本文件;
* 脚本文件是使用 Visual Basic .Net 或者 C# .Net 语言编写的;

脚本文件必须包含一个有public static/shared main 方法的Class或者Module;
脚本文件的语法总体来说和.vb或.cs文件相同;
示例文件放在了scripts文件夹中;

# 运行脚本文件

* 你可以双击脚本文件,这样它就会使用wnscript.exe(Windows模式)运行;
* 你可以右键单击脚本文件并选择"Open with Console Mode" 来使用cnscript.exe(控制台模式)运行;
* 对于处理大量数据的脚本来说,控制台模式的速度是Windows模式速度的1.5-2倍;

# 添加程序集引用

* 从1.4版本开始,你可以通过 #pragma comment(ref, "...") 语句添加程序集引用:
```
#pragma comment(ref, "system.web.dll")
#pragma comment(ref, "... other assembly ...")
```

# 重要提示

* 不要尝试修改Program Files中的示例脚本,需要先把他们复制到d:\或者其他的地方.
因为某些未知的原因(有可能是权限问题),在Program Files目录中运行包含错误的脚本文件会导致显示无关信息.
该信息仅包含一个编译器在打开dll文件时的异常信息.

# .NET框架帮助文档

* 官方文档: [.NET 4.0 帮助](https://docs.microsoft.com/en-us/dotnet/api/?view=netframework-4.0)
* VB.NET教程: [VB.NET 教程](https://www.w3cschool.cn/vb_net/)
* C#教程: [C#.NET 教程](https://www.w3cschool.cn/csharp/)