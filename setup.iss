#define MyAppExeBaseName1 "wnscript"
#define MyAppExeBaseName2 "cnscript"
#define MyAppExeBaseName3 "checkpath"
#define MyAppExeBaseName3Alias "findpath"
#define MyAppExeName1 "wnscript.exe"
#define MyAppExeName2 "cnscript.exe"
#define MyAppExeName3 "checkpath.exe"
#define MyAppName "NScript"
#define MyAppVersion "1.30"

[Setup]
AppName={#MyAppName}
AppVersion={#MyAppVersion}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
UninstallDisplayIcon={app}\ico1.ico
Compression=lzma2
SolidCompression=yes
OutputDir=.
OutputBaseFilename={#MyAppName}_{#MyAppVersion}_Setup
PrivilegesRequired=admin
LicenseFile=LICENSE
ArchitecturesInstallIn64BitMode=x64

[Files]
Source: "nscript\bin\x86\Release\nscript.dll"; DestDir: "{sys}"; Flags: 32bit 
Source: "wnscript\bin\x86\Release\wnscript.exe"; DestDir: "{sys}"; Flags: 32bit
Source: "wnscript\bin\x86\Release\wnscript.exe.config"; DestDir: "{sys}"; Flags: 32bit
Source: "cnscript\bin\x86\Release\cnscript.exe"; DestDir: "{sys}"; Flags: 32bit 
Source: "cnscript\bin\x86\Release\cnscript.exe.config"; DestDir: "{sys}"; Flags: 32bit
Source: "Ude.dll"; DestDir: "{sys}"; Flags: 32bit
Source: "checkpath\bin\x86\Release\checkpath.exe"; DestDir: "{sys}"; Flags: 32bit

Source: "nscript\bin\x64\Release\nscript.dll"; DestDir: "{sys}"; Flags: 64bit; Check: Is64BitInstallMode
Source: "wnscript\bin\x64\Release\wnscript.exe"; DestDir: "{sys}"; Flags: 64bit; Check: Is64BitInstallMode
Source: "wnscript\bin\x64\Release\wnscript.exe.config"; DestDir: "{sys}"; Flags: 64bit; Check: Is64BitInstallMode
Source: "cnscript\bin\x64\Release\cnscript.exe"; DestDir: "{sys}"; Flags: 64bit; Check: Is64BitInstallMode
Source: "cnscript\bin\x64\Release\cnscript.exe.config"; DestDir: "{sys}"; Flags: 64bit; Check: Is64BitInstallMode
Source: "Ude.dll"; DestDir: "{sys}"; Flags: 64bit; Check: Is64BitInstallMode
Source: "checkpath\bin\x64\Release\checkpath.exe"; DestDir: "{sys}"; Flags:64bit; Check: Is64BitInstallMode

Source: "ico1.ico"; DestDir: "{app}"
Source: "ico100.ico"; DestDir: "{app}"
Source: "ico101.ico"; DestDir: "{app}"
Source: "ico102.ico"; DestDir: "{app}"
Source: "ico324.ico"; DestDir: "{app}"
Source: "AddExtToEnv.vbx"; DestDir: "{app}"
Source: "README_en.md"; DestDir: "{app}"; DestName: "README.TXT"; Flags: isreadme
Source: "Help.url"; DestDir: "{app}"
Source: "scripts\*"; DestDir: "{app}\scripts"

[Icons]
Name: "{group}\HELP"; Filename: "{app}\HELP.url"; IconFilename: "{app}\ico324.ico";
Name: "{group}\README"; Filename: "{app}\README.TXT"
Name: "{group}\scripts"; Filename: "{app}\scripts"; 

[Registry]
Root: HKCR; Subkey: ".vbx"; Flags: uninsdeletekey
Root: HKCR; Subkey: ".vbx"; ValueType: string; ValueName: ""; ValueData: "VBXFile"
Root: HKCR; Subkey: "VBXFile"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\DefaultIcon"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\ico101.ico"; Flags: 
Root: HKCR; Subkey: "VBXFile\shell"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shell\open"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shell\open"; ValueType: string; ValueName: ""; ValueData: "Open with Windows Mode"
Root: HKCR; Subkey: "VBXFile\shell\open\command"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{sys}\{#MyAppExeName1}"" ""%1"" %*"; Flags:
Root: HKCR; Subkey: "VBXFile\shell\open2"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shell\open2"; ValueType: string; ValueName: ""; ValueData: "Open with Console Mode"
Root: HKCR; Subkey: "VBXFile\shell\open2\command"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shell\open2\command"; ValueType: string; ValueName: ""; ValueData: """{sys}\{#MyAppExeName2}"" ""%1"" %*"; Flags:
Root: HKCR; Subkey: "VBXFile\shell\open3"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\open3"; ValueType: string; ValueName: ""; ValueData: "Open with 32bit Windows Mode"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\open3\command"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\open3\command"; ValueType: string; ValueName: ""; ValueData: """{syswow64}\{#MyAppExeName1}"" ""%1"" %*"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\open4"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\open4"; ValueType: string; ValueName: ""; ValueData: "Open with 32bit Console Mode"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\open4\command"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\open4\command"; ValueType: string; ValueName: ""; ValueData: """{syswow64}\{#MyAppExeName2}"" ""%1"" %*"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "VBXFile\shell\edit"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shell\edit"; ValueType: string; ValueName: ""; ValueData: "Edit"
Root: HKCR; Subkey: "VBXFile\shell\edit\command"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shell\edit\command"; ValueType: string; ValueName: ""; ValueData: """{sys}\notepad.exe"" ""%1"""; Flags:
Root: HKCR; Subkey: "VBXFile\shellex"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shellex\DropHandler"; Flags: uninsdeletekey
Root: HKCR; Subkey: "VBXFile\shellex\DropHandler"; ValueType: string; ValueName: ""; ValueData: "{{60254CA5-953B-11CF-8C96-00AA00B8708C}"; Flags:

Root: HKCR; Subkey: ".csx"; Flags: uninsdeletekey
Root: HKCR; Subkey: ".csx"; ValueType: string; ValueName: ""; ValueData: "CSXFile"
Root: HKCR; Subkey: "CSXFile"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\DefaultIcon"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\ico101.ico"; Flags: 
Root: HKCR; Subkey: "CSXFile\shell"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shell\open"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shell\open"; ValueType: string; ValueName: ""; ValueData: "Open with Windows Mode"
Root: HKCR; Subkey: "CSXFile\shell\open\command"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{sys}\{#MyAppExeName1}"" ""%1"" %*"; Flags:
Root: HKCR; Subkey: "CSXFile\shell\open2"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shell\open2"; ValueType: string; ValueName: ""; ValueData: "Open with Console Mode"
Root: HKCR; Subkey: "CSXFile\shell\open2\command"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shell\open2\command"; ValueType: string; ValueName: ""; ValueData: """{sys}\{#MyAppExeName2}"" ""%1"" %*"; Flags:
Root: HKCR; Subkey: "CSXFile\shell\open3"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\open3"; ValueType: string; ValueName: ""; ValueData: "Open with 32bit Windows Mode"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\open3\command"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\open3\command"; ValueType: string; ValueName: ""; ValueData: """{syswow64}\{#MyAppExeName1}"" ""%1"" %*"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\open4"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\open4"; ValueType: string; ValueName: ""; ValueData: "Open with 32bit Console Mode"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\open4\command"; Flags: uninsdeletekey; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\open4\command"; ValueType: string; ValueName: ""; ValueData: """{syswow64}\{#MyAppExeName2}"" ""%1"" %*"; Check: Is64BitInstallMode
Root: HKCR; Subkey: "CSXFile\shell\edit"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shell\edit"; ValueType: string; ValueName: ""; ValueData: "Edit"
Root: HKCR; Subkey: "CSXFile\shell\edit\command"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shell\edit\command"; ValueType: string; ValueName: ""; ValueData: """{sys}\notepad.exe"" ""%1"""; Flags:
Root: HKCR; Subkey: "CSXFile\shellex"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shellex\DropHandler"; Flags: uninsdeletekey
Root: HKCR; Subkey: "CSXFile\shellex\DropHandler"; ValueType: string; ValueName: ""; ValueData: "{{60254CA5-953B-11CF-8C96-00AA00B8708C}"; Flags:

[Run]
Filename: "wnscript.exe"; Parameters: """{app}\AddExtToEnv.vbx"""

[Code]
function PrepareToInstall(var NeedsRestart: Boolean): String;
var ResultStr:String;    ResultCode:Integer;
begin
  if RegQueryStringValue(HKLM,'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1','UninstallString',ResultStr) then
  begin
    ResultStr := RemoveQuotes(ResultStr);
    Exec(ResultStr,'/silent','',SW_SHOWNORMAL,ewWaitUntilTerminated,ResultCode);
  end;
  if RegQueryStringValue(HKLM,'SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1','UninstallString',ResultStr) then
  begin
    ResultStr := RemoveQuotes(ResultStr);
    Exec(ResultStr,'/silent','',SW_SHOWNORMAL,ewWaitUntilTerminated,ResultCode);
  end;
  NeedsRestart := false;
  result := '';end;
 