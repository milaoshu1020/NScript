# NScript
Windows Script tool set based on. Net framework to replace VBS;

# Script Host And Relative Files

* wnscript.exe - .Net Windows Script Host;
* cnscript.exe - .Net Console Script Host;
* checkpath.exe - Check Command Filepath;

# Script Extension Names

* *.vbx - Visual Basic .Net Script File;
* *.csx - C# .Net Script File;

# Install NScript

* Requirements: [.Net Framework 4.0](https://www.microsoft.com/en-us/download/details.aspx?id=17718)
* [Download the setup file;](https://gitee.com/milaoshu1020/NScript/releases)
* Double click the NScript_XXX_Setup.exe;

# Edit Script File

* You can use notepad or other text editor to edit script file;
* The script is written in Visual Basic .Net or C# .Net;

It must contain a Class or Module with a public static/shared main method;
The syntax is totally the same as .vb or .cs file;
The example files are in the scripts folder; 

# Run Script File

* You can double click the script file to run with wnscript.exe (Windows Mode);
* You can right click the script file and select "Open with Console Mode" to run with cnscript.exe (Console Mode);
* For scripts that process large amounts of data, the speed of console mode is 1.5-2 times that of Windows mode;

# Add Referenced Assemblies

* From version 1.4, you can add referenced assemblies by adding #pragma comment(ref, "...") statement:
```
#pragma comment(ref, "system.web.dll")
#pragma comment(ref, "... other assembly ...")
```

# Important Notice

* Don't try to modify the example scripts in the program files, just copy them to the d:\ or some where else.
Because of some unknown reason (perhaps access forbidden), run the script file with some error in the program files directory would cause nothing useful to show, 
but only an exception information of compiler when it is opening a dll file;

# .NET Framework Help Document

* Official Document: [.NET 4.0 HELP](https://docs.microsoft.com/en-us/dotnet/api/?view=netframework-4.0)
