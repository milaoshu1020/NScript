#pragma comment(ref,"system.web.dll")

using System;
using System.Windows.Forms;
using System.Web;

class HelloWorld
{
	public static void main(string[] args)
	{
		string strTemp = HttpUtility.UrlEncode("Hello world!");
		Console.WriteLine(strTemp);
		Console.WriteLine(HttpUtility.UrlDecode(strTemp));
		Console.ReadKey();
	}
}
